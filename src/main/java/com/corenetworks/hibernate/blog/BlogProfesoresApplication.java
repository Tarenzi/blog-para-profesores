package com.corenetworks.hibernate.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogProfesoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogProfesoresApplication.class, args);
	}
}
