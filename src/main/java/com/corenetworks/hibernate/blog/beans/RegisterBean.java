package com.corenetworks.hibernate.blog.beans;

import java.util.Date;

/**
 * Un objeto simple que sirve de formulario para el registro de los usuarios.
 * */
public class RegisterBean {
	private String nombre;
	private String email;
	private Date fechaAlta;
	private String ciudad;
	private String password;
	private String secondPassword; //Confirmacion del password.
	
	public RegisterBean() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}
	
	
	
	
}
