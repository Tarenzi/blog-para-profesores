package com.corenetworks.hibernate.blog.beans;


/**
 * Objeto que funcionará para el cuestionario a la hora
 * del logueo del usuario con una cuenta ya existente.
 * 
 * */
public class LoginBean {
	private String email;
	private String password;

	public LoginBean() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
