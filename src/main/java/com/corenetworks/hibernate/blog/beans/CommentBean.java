package com.corenetworks.hibernate.blog.beans;

public class CommentBean {

	private String contenido;
	private long question_id;
	
	public CommentBean() {
		super();
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public long getPost_id() {
		return question_id;
	}

	public void setPost_id(long post_id) {
		this.question_id = post_id;
	}
	
	
	
	
	
}
