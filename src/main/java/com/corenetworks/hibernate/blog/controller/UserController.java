package com.corenetworks.hibernate.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.UserDao;


/**
 * Controlador encargado de devolver userlist.jsp para tener
 * una lista de todos los profesores registrados.
 * 
 * */
@Controller
public class UserController {
	
	@Autowired //Utilizaremos UserDao para leer de la base de datos los usuarios.
	private UserDao userDao;

	/**
	 * Lista todos los Usuarios registrados en la base de datos.
	 * 
	 * @return Devuelve userlist.jsp
	 * */
	@GetMapping(value = "/autores")
	public String listaAutores(Model modelo) {
		modelo.addAttribute("autores", userDao.getAll());
		return "userlist";
	}
}
