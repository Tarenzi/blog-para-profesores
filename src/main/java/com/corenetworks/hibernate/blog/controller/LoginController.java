package com.corenetworks.hibernate.blog.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.LoginBean;
import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.User;

/**
 * Controlador para el logueo de un usuario con una cuenta
 * ya existente.
 * 
 * Este controlador se encarga tanto del login como del logout.
 * 
 * En sus atributos posee un HttpSession, el cual es un objeto que nos permite crear una sesion
 * para que pueda ser rescatada mas tarde y no tener que estar logueando al cliente cada vez que
 * realiza una petición o una interacción.
 * */
@Controller
public class LoginController {
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private HttpSession httpSession; //Crear una sesion http para que el usuario logueado persista

	/**
	 * Cuando el cliente entre en /signin se le devolverá 
	 * el login.jsp
	 * 
	 * @return envia login.jsp para el logueo.
	 * */
	@GetMapping(value = "/signin")
	public String showForm(Model model) {
		model.addAttribute("userLogin", new LoginBean());
		return "login";
	}

	/**
	 * Logueará al usuario con email y password correctos y existentes en la base de datos.
	 * 
	 * @return si el usuario y password es correcto redireccionará al cliente a la raiz.
	 * @return si el cliente es erroneo volverá a devolver login.jsp
	 * */
	@PostMapping(value = "/login")
	public String submit(@ModelAttribute("userLogin") LoginBean loginBean, Model model) {
		// userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(),
		// r.getPassword()));
		User u = userDao.getByEmailAndPassword(loginBean.getEmail(), loginBean.getPassword());
		if (u != null) {
			httpSession.setAttribute("userLoggedIn", u);
			
		    return "redirect:/";
		} else {
			model.addAttribute("error","Error de validación");
			return "login";
		}
	}
	
	/**
	 * Desconexion del cliente
	 * 
	 * @return redirecciona al cliente a la raiz.
	 * */
	@GetMapping(value = "/logout")
	public String logout(Model model) {
		httpSession.removeAttribute("userLoggedIn");
		return "redirect:/";
	}
}
