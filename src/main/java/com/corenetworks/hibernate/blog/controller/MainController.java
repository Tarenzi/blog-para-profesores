package com.corenetworks.hibernate.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.QuestionDao;


/**
 * Clase controladora que envia al cliente al archivo index.jsp
 * 
 * Cuando el cliente llama al servidor con la ruta "/" este le responderá
 * enviandole a index.jsp
 * 
 * */
@Controller //Controlador
public class MainController {
	
	@Autowired
	QuestionDao questionDao;
	
	/**
	 * Metodo de bienvenida al cliente que llama a la ruta "/"
	 * respondiendole 
	 * 
	 * Además mostraremos todas las preguntas existentes en la base de datos para
	 * poder visualizarlas en el index.jsp
	 * 
	 * @return Dirige al cliente que llama a la ruta "/" hacia archivo index.jsp
	 */
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		//Mostraremos todas las preguntas existentes en el index.
		modelo.addAttribute("question", questionDao.getAll()); 
		
		return "index";
	}
}
