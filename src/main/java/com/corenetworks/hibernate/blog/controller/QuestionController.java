package com.corenetworks.hibernate.blog.controller;



import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.CommentBean;
import com.corenetworks.hibernate.blog.beans.QuestionBean;
import com.corenetworks.hibernate.blog.dao.CommentDao;
import com.corenetworks.hibernate.blog.dao.QuestionDao;
import com.corenetworks.hibernate.blog.model.Comment;
import com.corenetworks.hibernate.blog.model.Question;
import com.corenetworks.hibernate.blog.model.User;

/**
 * Controlador para crear preguntas y hacerlas persistentes
 * en la base de datos.
 * 
 * Solo los clientes registrados pueden hacer preguntas.
 * */

@Controller
public class QuestionController {
	@Autowired
	private QuestionDao questionDao;
	
	//HttpSession para rescatar la sesion del autor.
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CommentDao commentDao;
	
	/**
	 * showForm es invocado por el controlador cuando el cliente accede a /submit
	 * 
	 * Permite mostrar los post, añadiendo al modelo el post.
	 * 
	 */
	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("question", new QuestionBean());
		return "submit";
	}
	
	
	/**
	 * Submit es invocado por el controlador cuando el cliente accede a /submit/newpost
	 * 
	 * Permite crear un nuevo post, asociandolo a su autor, del cual se rescata
	 * su sesion para asignarlo como autor de dicho post.
	 * 
	 * Para ello, primero crea un objeto de tipo Post para asignarle los atributos del
	 * objeto PostBean.
	 * Después rescata la sesion de usuario utilizando HttpSession y asignando el objeto devuelto
	 * a un objeto Usuario llamado autor.
	 * 
	 * Por último añade al post el autor rescatado gracias a HttpSession y hace persistente el post
	 * utilizando el Objeto DAO llamado PostDao.
	 * 
	 * @param r el bean de Post
	 * @param model el modelo.
	 * @return una redireccion hacia la raiz.
	 * */
	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("question") QuestionBean r, Model model) {
		
         // Crear un Post
		
		/*Nuevo objeto Post usando los Sets para asignarle sus atributos. (el atributo de Usuario aun no puesto
		 que aun no rescatamos la sesion del usuario).*/
		Question question = new Question();
		question.setPregunta(r.getPregunta());
		question.setUrl(r.getUrl());
		
		 // Obtener el autor desde la sesión mediante la sesion.
		
		/*Obtenemos de esta manera la sesion del usuario, casteando el objeto devuelto por httpSession al
		  tipado User.*/
		User autorPost = (User) httpSession.getAttribute("userLoggedIn");
		
		 // Asignar el autor al post ya creado usando el SetAutor.
		question.setAutor(autorPost);
		
		//Hacemos persistente el post utilizando el Dao para acceder a la base de datos
		questionDao.create(question);
		autorPost.getQuestions().add(question);
		
		//A continuacion redireccionamos al usuario al directorio raiz.
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		Question result = null;
		
		if((result = questionDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			return "postdetails";
		} else {
			return "redirect:/";
		}
	}
	
	/**
	 * submitComment permite escribir comentario, asignando dicho comentario
	 * a sus respectivos Post y User.
	 * 
	 * @param commentBean es el bean del comentario
	 * @param model es el modelo.
	 * @return una direccion a /post/numero_post_id
	 */
	@PostMapping(value = "/submit/newComment")
	public String submitComment(@ModelAttribute("commemtForm") CommentBean commentBean, Model model) {
		
		User autor = (User) httpSession.getAttribute("userLoggedIn");

		Comment comment = new Comment();
		comment.setUser(autor);
		
		Question post = questionDao.getById(commentBean.getPost_id());
		comment.setQuestion(post);
		comment.setContenido(commentBean.getContenido());
		commentDao.create(comment);
		post.getComments().add(comment);
		autor.getComments().add(comment);
		
		return "redirect:/post/" + commentBean.getPost_id();
	}
	
	@GetMapping(value = "/submit/{id}")
	public String modifyForm(@PathVariable("id")  long id,Model modelo) {
		Question result = null;
		if ((result = questionDao.getById(id)) != null) {
			QuestionBean p = new QuestionBean();
			p.setPregunta(result.getPregunta());
			p.setUrl(result.getUrl());
			modelo.addAttribute("question", p);
			return "submit";			
		} else {
			return "redirect:/";
		}
	}
}
