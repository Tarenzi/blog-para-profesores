package com.corenetworks.hibernate.blog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;



/**
 * Entidad encargada de la persistencia de las preguntas de los profesores
 * en la base de datos.
 * */
@Entity
@Table(name="pregunta")
public class Question {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	private String url;
	
	@Column
	@Lob //Especifica que la persistencia será de un objeto de tamaño largo.
	private String pregunta;
	
	@Column
	@CreationTimestamp //Creará la fecha automaticamente (la del sistema)
	private Date fecha;

	//Muchas entidades Question para un mismo User (autor).
	@ManyToOne
	private User autor;
	
	@OneToMany(mappedBy="question")
	private List<Comment> comments = new ArrayList<>();
	
	public Question() {
		super();
	}

	public Question(String url, String pregunta) {
		super();
		this.url = url;
		this.pregunta = pregunta;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}	
	
	public void setAutor(User autor) {
		this.autor = autor;
		}
	public User getAutor() {
		return autor;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	
}
