package com.corenetworks.hibernate.blog.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

/**
 * Entidad User.
 * Esta entidad representa a los profesores registrados y hará que sus 
 * cuentas, identificadores, nombres, contraseñas, etc seán persistentes en la base de datos.
 */
@Entity
public class User {

	@Id // Será el identificador de la entidad
	@GeneratedValue(strategy=GenerationType.AUTO) //Generar un valor automaticamente
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column
	private String email;
	
	@Column
	@CreationTimestamp
	private Date fechaAlta;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;
	
	@Column
	private String ciudad;
	
	@OneToMany(mappedBy="autor", fetch= FetchType.EAGER)
	private Set<Question> questions = new HashSet<>();
	
	@OneToMany(mappedBy="user", fetch= FetchType.EAGER)
	private Set<Comment> comments = new HashSet<>();
	
	public User() {
		super();
	}

	public User(String nombre, String email, String password, String ciudad) {
		super();
		this.nombre = nombre;
		this.email = email;
		this.password = password;
		this.ciudad = ciudad;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	
	
}

