package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Question;


/**
 * Es el DAO (Database Access Object) encargado de la relacion
 * entre el objeto Question y la base de datos.
 * 
 * Nos permitirá hacer persistente una pregunta en la base de datos,
 * retornarnos una lista de todas las preguntas y conseguir una pregunta
 * dependiendo de su numero ID.
 * 
 * */
@Repository
@Transactional
public class QuestionDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    
    /**
     * Hace posible la persistencia de la pregunta en la base de datos
     * encargado por entityManager.
     * 
     * @param Se pasa por parametros un objeto Question para hacer persistente.
     */
    public void create(Question question) {
    	entityManager.persist(question);
    	return;
    }
    
    /**
     * Devolverá una lista con todas las preguntas en la base de datos.
     * 
     * @return Una lista con todas las preguntas.
     * */
    @SuppressWarnings("unchecked")
	public List<Question> getAll(){
    	return entityManager
    			.createQuery("select q from Question q")
    			.getResultList();
    }
    
    /**
     * EntityManager busca en la base de datos un post con una id introducida
     * para retornar un post segun la id.
     * 
     * @param id la ID que se busca.
     * @return retorna una pregunta con un atributo ID dado.
     * 
     * */
    public Question getById(long id) {
    		return entityManager.find(Question.class, id);
    }
 
    
    
    
}
