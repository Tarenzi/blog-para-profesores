package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.User;


/**
 * UserDao es la clase encargada de hacer persistente los usuarios en la base de datos
 * y de rescatar los datos del mismo.
 * 
 * Además poseerá un metodo para conseguir todos los usuarios a la vez y para 
 * retornar un usuario segun su email y su password.
 * 
 * Los objetos DAO son los encargados de acceder a la base de datos para interaccionar con ella
 * DAO significa = Database Access Object, es decir, objeto con acceso a base de datos.
 * 
 * */
@Repository
@Transactional
public class UserDao {
	
	//La entidad que accederá a la base de datos
    @PersistenceContext
	private EntityManager entityManager;
   
    
    /**
     * Hace persistente con el objeto entityManager al usuario introducido
     * en sus parametros
     * 
     * @param introducimos al usuario que será en la base de datos.
     * 
     * */
    public void create(User usuario) {
    	entityManager.persist(usuario);
    	return;
    }
    
    /**
     * Devolverá todos los usuarios de la tabla User.
     * 
     * @return una lista con todos los user.
     * */
    @SuppressWarnings("unchecked")
	public List<User> getAll(){
    	
    	return entityManager
    			.createQuery("select u from User u") //Es una peticion de Hibernate que es una peticion a todos los objetos, no a la base de datos.
    			.getResultList();
    }
    
    
    /**
     * Validará el password asociado al email del usuario que quiere loguearse buscando
     * dentro de la base de datos una relación entre el email introducido y el password.
     * 
     * Si no se encuentra devolverá un objeto null.
     * 
     * @param Se le pasará como parametros el email del usuario a loguear y su password.
     * @return Si hay una asociacion retorna el usuario.
     * @return Si no existe devuelve un objeto de tipo User null.
     */
    public User getByEmailAndPassword(String email, String password) {
    	User resultado = null;
    	try {
    	resultado = (User)  entityManager
    	    .createNativeQuery("select * FROM user where  email= :email and password=md5(:password)", //createNativeQuery es una peticion directa a la base de datos.
    	    		User.class)
    	     .setParameter("email", email)
    	     .setParameter("password", password)
    	     .getSingleResult();
    	} catch (NoResultException e) {
    		resultado = null;
		}
    	return resultado;
    	
    }
    
    
    
}

