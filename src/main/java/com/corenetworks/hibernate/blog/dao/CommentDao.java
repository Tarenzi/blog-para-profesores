package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Comment;

@Repository
@Transactional
public class CommentDao {

	private EntityManager entityManager;
	
	/**
	 * Hace persistente un comentario en la base de datos.
	 * 
	 * @param comment Comentario que queremos hacer persistente en la base de datos.
	 * @return void.
	 * */
	public void create(Comment comment) {
		
		entityManager.persist(comment);
		return;
	}
	
	@SuppressWarnings("unchecked")
	public List<Comment> getAll(){
		
		return entityManager.createQuery("select c from Comment c")
				.getResultList();
	}

	/**
	 * Conseguir un comentario mediante un numero idenfiticativo.
	 * 
	 * @param id Pasamos un numero id, el cual buscará entre los comentarios.
	 * @return Devuelve un objeto del tipo Comment al introducirle un id
	 * */
	public Comment getById(long id) {
		
		return entityManager.find(Comment.class, id);
	}
}

