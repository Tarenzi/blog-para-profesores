package com.corenetworks.hibernate.blog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * La configuracion Web. Extendiendo WebMvcConfigureAdapter
 * 
 * */
@Configuration //Configuracion 
@EnableWebMvc //Importa la configuracion MVC de Spring
public class WebConfig extends WebMvcConfigurerAdapter {

	/**
	 * Con este método addResourceHandler estaremos asignando
	 * donde se encuentran los manejadores de los recursos.
	 * 
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
			.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry
			.addResourceHandler("/assets/**")
			.addResourceLocations("/assets/");
		
		}
	
	/**
	 * El método getViewResolver resolverá el final de los archivos colocando .jsp
	 * a los archivos a los que el usuario llegue para visualizarlos.
	 * 
	 * @return retorna un objeto que resuelve las direcciones internamente.
	 * 
	 * */
	@Bean
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver resolver
			= new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		
		return resolver;
	}
	
	}

