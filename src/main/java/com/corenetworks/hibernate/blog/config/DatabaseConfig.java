package com.corenetworks.hibernate.blog.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * Clase encargada de la configuración de la conexion a la base de datos.
 * 
 * Contiene atributos autocableados (@Autowired) como son:
 * -Environment
 * -DataSource
 * -LocalContainerEntityManagerFactoryBean
 */
@Configuration //Configuración.
@EnableTransactionManagement //Gestor de transacciones.
public class DatabaseConfig {
	/*
	 * Definicion del DataSource para la conexion a nuestra
	 * base de datos. 
	 * Las propiedades son establecidas desde el fichero:
	 * application.properties y asignadas usando el objeto:
	 * env.
	 * 
	 */
	
	//Representa el Entorno donde en el cual la aplicacion está corriendo.
	@Autowired
	private Environment env;
	
	//DataSource es una fabrica para conexiones con la base de datos
	@Autowired
	private DataSource dataSource;
	
	//Creará un JPA local de persistencia.
	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;
	
	/**
	 * El método dataSource define la conexión con la base de datos.
	 * Las propiedades son establecidas por en fichero src/main/sources/application.properties
	 * 
	 * Las propiedades se asignan desde application.properties al objeto DriverManagerDataSource
	 * gracias al atributo Environment llamado env.
	 * 
	 * Environment accede al archivo application.properties y recoge la informacion para asignarle a
	 * DiverManagerDataSource el driver, la url, el username de acceso a la base de datos y el password.
	 * 
	 * @return Retorna un objeto DataSource.
	 * */
	@Bean 
	public DataSource dataSource() { 
		/*
		 * Aqui estamos pasandole al dataSource con los set los valores
		 * de application.properties. 
		 * 
		 * */
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("db.driver"));
		dataSource.setUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		return dataSource;
		
	}
	
	/**
	 * entityManagerFactory creará e inicializará un EntityManagerFactory de JPA
	 * para la persistencia de las entidades.
	 * 
	 * @return Un objeto LocalContainerEntityManagerFactoryBean
	 * 
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		
		//Creamos un objeto LocalContainerEntityManagerFactoryBean:
		LocalContainerEntityManagerFactoryBean entityManagerFactory = 
				new LocalContainerEntityManagerFactoryBean();
		
		//Asigna el dataSource que contiene la informacion de la base de datos al entityManagerFactory:
		entityManagerFactory.setDataSource(dataSource);
		
		// Indicamos la ruta donde tiene que buscar las clases con anotaciones:
		entityManagerFactory.setPackagesToScan(env.getProperty("entityManager.packagesToScan"));
		
		//Unimos JPA con Hibernate:
		HibernateJpaVendorAdapter vendorAdapter =
				new HibernateJpaVendorAdapter();
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		
		//Propiedades de hibernate:
		Properties additionalProperties = new Properties();
		additionalProperties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		additionalProperties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		additionalProperties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		
		//Asignar al entityManagerFactory las propiedades de JPA:
		entityManagerFactory.setJpaProperties(additionalProperties); 
		
		return entityManagerFactory;
	}
	
	/**
	 * Crea e inicializa un gestor de transacciones JPA.
	 * 
	 * @return Devuelve un objeto JpaTransactionManager al cual se le asignó el atributo entityManagerFactory.
	 */
	@Bean
	public JpaTransactionManager transactionManager() {
		
		//Creamos e inicializamos el objeto JpaTransactionManager.
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		
		//Asignamos al objeto el atributo de la clase entityManagerFactory.
		transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
		
		return transactionManager;
	}
	

	/**
	 * Creamos un Bean que va a ser un postprocessor que ayuda
	 * a relanzar excepciones especificas de cada plataforma en
	 * aquellas clases que vamos a anotar con @Repository 
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
}
