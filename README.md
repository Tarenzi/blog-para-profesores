# README #

Proyecto en el que desarrollaremos un blog en el cual una serie de profesores tendrán la capacidad de subir preguntas con sus respuestas, editarlas, o borrarlas.

### ¿Qué Frameworks y herramientas utilizaremos? ###
	
* El proyecto incorporará:
	Spring MVC
	Maven
	Hibernate
	
* Con el uso de Spring MVC facilitaremos la utilización del patrón de diseño Modelo - Vista - Controlador.

* Hibernate es una herramienta ORM, es decir, para el mapeo objeto-relacional. Hibernate nos provee de una serie de etiquetas para este fin.

* Maven nos ayudará en la gestion y construccion de proyectos Java. 
  Maven utiliza un Project Object Model (POM) para describir el proyecto de software a construir, sus dependencias de otros módulos y componentes externos, y el orden de construcción de los elementos.

### Características de la aplicación ###

* Los profesores poseerán cuentas personales con las que subiran preuguntas.
* Los profesores podrán asignar a las preguntas unas respuestas (la pregunta correcta irá resaltada en negrita).
* Los Usuarios sin cuenta (los alumnos) podrán ver las preguntas y respuestas subidas por los profesores.
* Además, los profesores podrán borrar y editar las preguntas y respuestas.

### Caracteristicas técnicas de la aplicación ###

* La aplicación poseerá una base de datos donde guardará las entidades de la aplicacion (es decir, los objetos persistentes)
* El front-end estará desarrollado en JSP, además de utilizar CSS y JavaScript.
* Utilizaremos el patron de diseño MVC, es decir, modelo - vista - controlador.

La aplicación poseerá objetos del MVC.
* Models: es decir, los modelos, los cuales serán entidades persistentes en la base de datos.
* Controllers: Los controladores con los que el usuario podrá interaccionar.
* Vista: lo que el usuario vé, es decir, archivos JSP.

La aplicacion contendrá además:
* Objetos DAO. Objetos con acceso a la base de datos. Estos objetos son los que realizarán cambios persistentes en la base de datos o los que devuelven valores de ella.
* DatabaseConfig: la configuración de la base de datos.
* WebConfig: la configuracion web.
* Beans: POJO's con los que los controladores interaccionarán.

### Creador ###

* Jesús Hernández Puro.